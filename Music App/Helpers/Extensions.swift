//
//  Extensions.swift
//  Music App
//
//  Created by Agung Setiawan on 21/04/21.
//

import UIKit

extension UICollectionViewCell {
    static func register(for collectionView: UICollectionView)  {
        let cellName = String(describing: self)
        let cellIdentifier = cellName
        let cellNib = UINib(nibName: String(describing: self), bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: cellIdentifier)
    }
}

