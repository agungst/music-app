//
//  CommonHelper.swift
//  Music App
//
//  Created by Agung Setiawan on 21/04/21.
//

import UIKit
import Alamofire
import SwifterSwift

class CommonHelper {
    
    // MARK: - Common Functions
    static let shared = CommonHelper()
    private init() {} //This prevents others from using the default '()' initializer for this class.
    
    func isConnectedToInternet() -> Bool {
        let networkReachabilityManager = NetworkReachabilityManager()
        return networkReachabilityManager?.isReachable ?? false
    }
    
    func testFunction() {
        // Function
    }
    
    func buildPlayerViewController() -> PlayerViewController {
        let view = PlayerViewController(nibName: String(describing: PlayerViewController.self), bundle: nil)
        let interactor = PlayerInteractor()
        let router = PlayerRouter()
        let presenter = PlayerPresenter(interface: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.output = presenter
        router.viewController = view
        return view
    }
        
}
