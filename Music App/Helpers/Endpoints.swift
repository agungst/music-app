//
//  Endpoints.swift
//  Music App
//
//  Created by Agung Setiawan on 21/04/21.
//

import Foundation

private protocol EndpointType {
    var url: String { get }
}

private struct Api {
    static let baseUrl = "https://itunes.apple.com"
}


enum Endpoints {
    
    enum App: EndpointType {
        
        case search

        public var url: String {
            switch self {
            case .search: return "\(Api.baseUrl)/search"
            }
        }
        
    }

}
