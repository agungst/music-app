//
//  CollectionViewCell.swift
//  Music App
//
//  Created by Agung Setiawan on 21/04/21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var trackLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var albumLabel: UILabel!
    
    var track: PlayerEntityModel? {
        didSet {
            guard let track = track else { return }
            
            if let url = URL(string: track.artworkUrl60) {
                imageView.kf.setImage(with: url)
            }
            
            trackLabel.text = track.trackName
            artistLabel.text = track.artistName
            albumLabel.text = track.collectionName
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.kf.indicatorType = .activity
    }
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? UIColor.gray : UIColor.white
        }
    }

}
