//
//  PlayerUseCase.swift
//  Music App
//
//  Created by Agung Setiawan on 21/04/21.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftyUserDefaults

protocol PlayerUseCaseDelegate: class {
    func success(_ object: PlayerEntity)
    func failed()
}

class PlayerUseCase: ApiInterface {
    var method: HTTPMethod?
    
    var url: URLConvertible
    
    var headers: HTTPHeaders?
    
    var parameters: Parameters?
    
    var encoding: ParameterEncoding?
    
    var requestHandler: PlayerUseCaseDelegate?
    
    
    // MARK: Inits
    required init() {
        method = .get
        url = Endpoints.App.search.url
    }
    
    func start() {
        ApiHelper.apiRequest(api: self)
    }
    
    func prepare(term: String, requestHandler: PlayerUseCaseDelegate) {
        self.requestHandler = requestHandler
        parameters = [
            "term": term,
            "entity": "musicTrack"
        ]
    }
        
    func success(_ value: JSON) {
        createObject(value)
    }
    
    func failed(_ value: JSON?) {
        requestHandler?.failed()
    }
    
    func createObject(_ value: Any) {
        guard let json = value as? JSON else { return }
        if let dict = json.dictionaryObject, let response = PlayerEntity(JSON: dict) {
            requestHandler?.success(response)
        } else {
            failed(nil)
        }
    }
}
