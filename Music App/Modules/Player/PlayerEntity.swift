//
//  PlayerEntity.swift
//  Music App
//
//  Created by Agung Setiawan on 21/04/21.
//

import Foundation
import ObjectMapper
import SwiftyUserDefaults

class PlayerEntity: Mappable {
    var results: [PlayerEntityModel]?
    var resultCount = 0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        results <- map["results"]
        resultCount <- map["resultCount"]
    }

}

class PlayerEntityModel: Mappable {
    var trackId = 0
    var trackName = ""
    var artistName = ""
    var collectionName = ""
    var artworkUrl60 = ""
    var previewUrl = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        trackId <- map["trackId"]
        trackName <- map["trackName"]
        artistName <- map["artistName"]
        collectionName <- map["collectionName"]
        artworkUrl60 <- map["artworkUrl60"]
        previewUrl <- map["previewUrl"]
    }

}
