//
//  PlayerInteractor.swift
//  Music App
//
//  Created by Agung Setiawan on 21/04/21.
//

import Foundation

class PlayerInteractor: PlayerInteractorInputProtocol {
    
    weak var output: PlayerInteractorOutputProtocol?
    
    let useCase = PlayerUseCase()

    func fetchData(term: String) {
        useCase.prepare(term: term, requestHandler: self)
        useCase.start()
    }
    
}

extension PlayerInteractor: PlayerUseCaseDelegate {
    
    func success(_ object: PlayerEntity) {
        output?.requestDidSuccess(object)
    }
    
    func failed() {
        output?.requestDidFailed(message: "Failed to fetch data")
    }
    
}
