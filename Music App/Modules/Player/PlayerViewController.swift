//
//  PlayerViewController.swift
//  Music App
//
//  Created by Agung Setiawan on 21/04/21.
//

import UIKit
import PKHUD
import AVFoundation

class PlayerViewController: UIViewController {
    
    @IBOutlet weak var colView: UICollectionView!
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var playButton: UIButton!
    
    var presenter: PlayerPresenterProtocol?
    var player: AVPlayer?

    lazy var searchBar: UISearchBar = UISearchBar()
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        presenter?.viewDidLoad()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - publics
    // Add public functions here

    // MARK: - IBActions
    // Add IBActions in this area
    @IBAction func playTapped(_ sender: Any) {
        togglePlay()
    }
    
    // MARK: - Privates
    private func setupViews() {
        searchBar.placeholder = "Search here..."
        searchBar.delegate = self
        navigationItem.titleView = searchBar
        
        CollectionViewCell.register(for: colView)
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = mediaView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mediaView.addSubview(blurEffectView)
        mediaView.bringSubviewToFront(playButton)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    private func togglePlay() {
        guard let player = player else { return }
        if player.timeControlStatus == .playing {
            player.pause()
            playButton.isSelected = false
        } else {
            player.play()
            playButton.isSelected = true
        }
    }
    
    @objc private func playerDidFinishPlaying(note: NSNotification) {
        mediaView.isHidden = true
    }
    
}

// MARK: - View Protocol
extension PlayerViewController: PlayerViewProtocol {
    
    func playMusic(url: URL) {
        player = AVPlayer(url: url)
        player?.play()
        mediaView.isHidden = false
        playButton.isSelected = true
    }
    
    func populateData() {
        colView.reloadData()
    }

    func showProgressDialog(show: Bool) {
        DispatchQueue.main.async {
            show ? HUD.show(.progress) : HUD.hide()
        }
    }

    func showAlertMessage(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlert(title: title, message: message)
        }
    }

    func showErrorMessage(message: String) {
        DispatchQueue.main.async {
            self.showAlert(title: nil, message: message)
        }
    }
}

extension PlayerViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        guard let text = searchBar.text else { return }
        presenter?.search(term: text)
    }
    
}

extension PlayerViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.entity?.resultCount ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CollectionViewCell.self), for: indexPath) as! CollectionViewCell
        cell.track = presenter?.entity?.results?[safe: indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 108)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let track = presenter?.entity?.results?[safe: indexPath.item] else { return }
        presenter?.play(track: track)
    }
    
}
