//
//  PlayerContract.swift
//  Music App
//
//  Created by Agung Setiawan on 21/04/21.
//

import Foundation

protocol PlayerRouterProtocol: class {

}

protocol PlayerPresenterProtocol: class {
    
    var entity: PlayerEntity? { get set } 
    
    func viewDidLoad()
    func search(term: String)
    func play(track: PlayerEntityModel)
    func pause()

}

protocol PlayerInteractorOutputProtocol: class {
    
    func requestDidSuccess(_ object: PlayerEntity)
    func requestDidFailed(message: String)
}

protocol PlayerInteractorInputProtocol: class {
    
    var output: PlayerInteractorOutputProtocol? { get set }

    func fetchData(term: String)
    
}

protocol PlayerViewProtocol: class {
    
    // Presenter -> ViewController
    func playMusic(url: URL)
    func populateData()
    func showProgressDialog(show: Bool)
    func showAlertMessage(title: String, message: String)
    func showErrorMessage(message: String)
    
}
