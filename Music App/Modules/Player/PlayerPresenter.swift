//
//  PlayerPresenter.swift
//  Music App
//
//  Created by Agung Setiawan on 21/04/21.
//

import Foundation

class PlayerPresenter: PlayerPresenterProtocol {
    
    weak private var view: PlayerViewProtocol?
    private let interactor: PlayerInteractorInputProtocol?
    private let router: PlayerRouterProtocol?

    var entity: PlayerEntity?
    
    init(interface: PlayerViewProtocol, interactor: PlayerInteractorInputProtocol?, router: PlayerRouterProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    // MARK: PresenterProtocol

    func viewDidLoad() {
        // TODO: Add on view loaded
    }
    
    func search(term: String) {
        view?.showProgressDialog(show: true)
        interactor?.fetchData(term: term)
    }
    
    func play(track: PlayerEntityModel) {
        guard let url = URL(string: track.previewUrl) else { return }
        view?.playMusic(url: url)
    }
    
    func pause() {
        
    }

}

extension PlayerPresenter: PlayerInteractorOutputProtocol {

    // MARK: InteractorOutputProtocol

    func requestDidSuccess(_ object: PlayerEntity) {
        entity = object
        view?.showProgressDialog(show: false)
        view?.populateData()
    }

    func requestDidFailed(message: String) {
        view?.showProgressDialog(show: false)
        view?.showErrorMessage(message: message)
    }

}
